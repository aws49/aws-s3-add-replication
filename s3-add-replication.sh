#!/bin/bash
# 
# title         : s3-add-replication.sh
# description   : This script for add replication to AWS S3 Bucket.
# author        : Oleksii Pavliuk pavliuk.aleksey@gmail.com
# date          : 06/30/2020
# version       : 1.2.0
# usage         : Usage: ./s3-add-replication [OPTIONS]
# notes         : Before using this script, sure that you have full-access
#                 permissions to AWS IAM and AWS S3.
#                 Creates replication without replication Delete Markers.
#                 Creates configuration files for the role if files don't exist:
#                     - ./s3-role-trust-policy.json
#                     - ./s3-role-policy-$SRC_BUCKET.json
#                     - ./s3-replication-$SRC_BUCKET.json
#                 
#                 [!] The script wrote on the Mac OS system and can have
#                     undefined behavior on another operating system.
#

#
# -e: option instructs bash to immediately exit if
# any command has a non-zero exit status.
#
set -e

#
# Global variables.
#
SRC_BUCKET=""
DEST_BUCKET=""

REPLICATION_ROLE="ReplicationRoleS3"                # replicationRoleS3-$SRC_BUCKET
REPLICATION_ROLE_CONFIG="s3-role-trust-policy.json"
REPLICATION_ROLE_POLICY="ReplicationRoleS3Policy"   # replicationRoleS3Policy-$SRC_BUCKET
REPLICATION_ROLE_POLICY_CONFIG="s3-role-policy"     # s3-role-policy-$SRC_BUCKET.json
REPLICATION_CONFIG="s3-replication"                 # s3-replication-$SRC_BUCKET.json

AWS_REGION=""
CLI_PROFILE="default"
LOG_FILE_PATH="./s3-add-replication.log"

WITHOUT_APPROVE=""

cmdname=$(basename $0)

#
# ERROR Logging.
# Logging to syslog and console output.
#
# Arguments: $@ - error messages
# Return   : None
#
echoerr() {
    date=`date`
    # echo "$date $cmdname $@" >> $LOG_FILE_PATH
    echo "$@" 1>&2;
}

#
# INFO Logging.
# Logging to syslog and console output.
#
# Arguments: $@ - log messages
# Return   : None
#
log() {
    date=`date`
    # echo "$date $cmdname $@" >> $LOG_FILE_PATH
    echo "$@"
}

#
# Usage.
# Show variables which you must or can set like arguments.
#
# Arguments: None
# Return   : None
#
usage() {
    cat << USAGE >&2
Usage: ./$cmdname [OPTIONS]
  OPTIONS:
     [required]:
        --src-bucket ARG              : a name of source bucket, which we want to replicate
        --dest-bucket ARG             : a name of destination bucket, it is replica
     [optional]:
        --aws-region ARG              : AWS Region [default: $AWS_REGION]
        --cli-profile ARG             : profile in AWS CLI [default: $CLI_PROFILE]
        --log-file-path ARG           : path to log file for logging [default: $LOG_FILE_PATH]
        --without-approve             : do all processes without user approve:
                                          * create buckets;
                                          * enable versioning;
                                          * create role;
                                          * attach policy to role.
        -h | --help                   : show this usage

Example: ./$cmdname --src-bucket SRC_BUCKET --dest-bucket DEST_BUCKET
USAGE
    exit 1
}

#
# Checking connection to AWS S3.
#
# Arguments: None
# Return   : None
#
check_aws_s3_connection() {
    LOG_CASC_1="==> Connecting to AWS S3..."
    LOG_CASC_2="<== [!] Connected."

    log "$LOG_CASC_1"
    aws s3 ls 1>/dev/null
    log "$LOG_CASC_2"
}

#
# Checking connection to AWS IAM.
#
# Arguments: None
# Return   : None
#
check_aws_iam_connection() {
    LOG_CAIC_1="==> Connecting to AWS IAM..."
    LOG_CAIC_2="<== [!] Connected."

    log "$LOG_CAIC_1"
    aws iam list-roles 1>/dev/null
    log "$LOG_CAIC_2"
}

#
# Checking connection to AWS Services.
#
# Arguments: None
# Return   : None
#
check_aws_connection() {
    LOG_CAC_1="=> Checking connection to AWS Services..."
    LOG_CAC_2="<= [!] Connections passed successfully."

    log "$LOG_CAC_1"
    check_aws_s3_connection
    check_aws_iam_connection
    log "$LOG_CAC_2"
}

#
# Request to enable bucket versioning to AWS S3.
#
# Arguments: $1 - a bucket name
# Return   : None
#
request_enable_versioning() {
    LOG_REV_1=">>> Requesting enable versioning for bucket \
[$1] to AWS S3..."
    LOG_REV_2="<<< [!] SUCCESS >>>"

    log "$LOG_REV_1"
    aws s3api put-bucket-versioning \
        --bucket $1 \
        --versioning-configuration Status="Enabled" \
        --profile $CLI_PROFILE
    log "$LOG_REV_2"
}

#
# Request to block bucket public access to AWS S3.
#
# Arguments: $1 - a bucket name
# Return   : None
#
request_block_public_access() {
    LOG_RBPA_1=">>> Requesting block public access for \
bucket [$1] to AWS S3..."
    LOG_RBPA_2="<<< [!] SUCCESS >>>"

    log "$LOG_RBPA_1"
    aws s3api put-public-access-block \
        --bucket $1 \
        --public-access-block-configuration """BlockPublicAcls=true,
                                               IgnorePublicAcls=true,
                                               BlockPublicPolicy=true,
                                               RestrictPublicBuckets=true"""
    log "$LOG_RBPA_2"
}

#
# Request to create bucket to AWS S3.
#
# Arguments: $1 - a bucket name
# Return   : None
#
request_create_bucket() {
    LOG_RCB_1=">>> Requesting create bucket [$1] in the \
region [$AWS_REGION] to AWS S3..."
    LOG_RCB_2="<<< [!] SUCCESS >>>"

    log "$LOG_RCB_1"
    aws s3 mb s3://$1 \
        --region $AWS_REGION \
        --profile $CLI_PROFILE
    log "$LOG_RCB_2"
}

#
# Request to create role to AWS IAM.
#
# Arguments: $1 - a replication role name
# Return   : None
#
request_create_role() {
    LOG_RCB_1=">>> Requesting create role [$1] to AWS IAM..."
    LOG_RCB_2="<<< [!] SUCCESS >>>"

    policy_doc="file://$2"

    log "$LOG_RCB_1"
    aws iam create-role \
        --role-name $1 \
        --assume-role-policy-document $policy_doc  \
        --profile $CLI_PROFILE
    log "$LOG_RCB_2"
}

#
# Request to attach replication policy to role in AWS IAM.
#
# Arguments: $1 - a file name/path of config file
#                 for attach policy request
#            $2 - a policy name
#            $3 - a role name
# Return   : None
#
request_attach_policy() {
    LOG_RAP_1=">>> Requesting attach policy [$2] role [$3] to AWS IAM..."
    LOG_RAP_2="<<< [!] SUCCESS >>>"

    policy_doc="file://$1"

    log "$LOG_RAP_1"
    aws iam put-role-policy \
        --role-name $3 \
        --policy-document $policy_doc \
        --policy-name $2 \
        --profile $CLI_PROFILE
    log "$LOG_RAP_2"
}

#
# Request to synchronize buckets in AWS S3.
#
# Arguments: $1 - a source bucket name
#            $2 - a destination bucket name
# Return   : None
#
request_sync_buckets() {
    LOG_RSB_1=">>> Requesting synchronize a source bucket [$1] \
and a destination bucket [$2]"
    LOG_RSB_2="<<< [!] SUCCESS >>>"

    log "$LOG_RSB_1"
    time aws s3 sync s3://$1 s3://$2
    log "$LOG_RSB_2"
}

#
# Request about bucket size to AWS S3.
#
# Arguments: $1 - a bucket name.
# Return   : Array(num_of_objects, bytes_size)
#
request_bucket_size() {
    bucket_size=$(aws s3api list-objects \
                      --bucket $1 \
                      --output=yaml \
                      --query 'sum(Contents[].Size)')
    bucket_obj_num=$(aws s3api list-objects \
                         --bucket $1 \
                         --output=yaml \
                         --query 'length(Contents[])')
    echo "$bucket_obj_num" "$bucket_size"
}

#
# Request to add replication to AWS S3.
#
# Arguments: $1 - a file name/path of config file
#                 for add replication to bucket request
#            $2 - a bucket name
# Return   : None
#
request_add_replication() {
    LOG_RAR_1=">>> Requesting add replication to source bucket [$2] to AWS S3..."
    LOG_RAR_2="<<< [!] SUCCESS >>>"

    policy_doc="file://$1"

    log "$LOG_RAR_1"
    aws s3api put-bucket-replication \
        --replication-configuration $policy_doc \
        --bucket $2 \
        --profile $CLI_PROFILE
    log "$LOG_RAR_2"
}

#
# Approving request to user.
# Asks user to approve request.
# Uses only 'Y' or 'n'.
#
# Arguments: $1 - the main message
# Return   : true or false
#
approve_request() {
    LOG_AR_2="Please, use only 'Y' or 'n' to confirm! [Y/n]: "

    read -p "$1 $LOG_AR_2" var
    while True; do
        if [ $var == "n" ];
        then
            echo "false"
            return
        elif [ $var == "Y" ];
        then
            echo "true"
            return
        else
            read -p "$LOG_AR_2" var
        fi
    done
}

#
# Approving to create bucket in AWS S3.
#
# Arguments: $1 - a bucket name
# Return   : None
#
create_bucket_user_approving() {
    LOG_CBUA_1="""Do you want to create bucket [$1] in AWS S3? \
It needs for the process of adding replication \
to bucket. If it doesn't exist, the process will stop."""
    LOG_CBUA_2="[ERROR] The bucket [$1] DOESN'T exist in AWS S3. \
You need to create it to continue the process adding replication."

    if [ ! -z $WITHOUT_APPROVE ];
    then
        return
    fi

    res=$(approve_request "$LOG_CBUA_1")
    if [ $res == "false" ];
    then
        echoerr "$LOG_CBUA_2"
        exit 1
    fi
}

#
# Creating bucket in AWS S3.
# Asks user Approve before create it.
#
# Arguments: $1 - a bucket name
# Return   : None
#
create_bucket()
{
    LOG_CRB_1="===> Creating bucket [$1] without public access is started..."
    LOG_CRB_2="<=== [!] The bucket [$1] without public access is created successfully."

    create_bucket_user_approving $1
    log "$LOG_CRB_1"
    request_create_bucket $1
    request_block_public_access $1
    log "$LOG_CRB_2"
}

#
# Approving to create replication role in AWS IAM.
#
# Arguments: $1 - a role name
# Return   : None
#
create_role_user_approving() {
    LOG_CRUA_1="""Do you want to create role [$1] in AWS IAM? \
It needs for the process of adding replication \
to bucket. If it doesn't exist, the process will stop."""
    LOG_CRUA_2="[ERROR] The role [$1] DOESN'T exist in AWS IAM. \
You need to create it to continue the process adding replication."

    if [ ! -z $WITHOUT_APPROVE ];
    then
        return
    fi

    res=$(approve_request "$LOG_CRUA_1")
    if [ $res == "false" ];
    then
        echoerr "$LOG_CRUA_2"
        exit 1
    fi
}

#
# Creating config file for replication role request.
# Path to file: ./{$REPLICATION_ROLE_CONFIG}
#
# Arguments: $1 - a name of config file
# Return   : None
#
create_role_config() {
    LOG_CRRC_1="=====> Creating new config file [$1]..."
    LOG_CRRC_2="<===== [!] Config file [./$1] is created."

    config_file="""
{
  \"Version\":\"2012-10-17\",
  \"Statement\":[
    {
      \"Effect\": \"Allow\",
      \"Principal\": {
        \"Service\": \"s3.amazonaws.com\"
      },
      \"Action\": \"sts:AssumeRole\"
    }
  ]
}
"""
    log "$LOG_CRRC_1"
    echo $config_file > ./$1
    log "$LOG_CRRC_2"
}

#
# Checking existing config file for replication role.
# If not, it will be created automated.
#
# Arguments: $1 - a name of config file
# Return   : None
#
check_role_config() {
    LOG_CRC_1="====> Checking exist config file [./$1] for \
replication role or not..."
    LOG_CRC_2="<==== [!] Config file [./$1] for role request already exists."
    LOG_CRC_3="====> [!] Can't found file [./$1]."

    log "$LOG_CRC_1"
    if [ -f "./$1" ];
    then
        log "$LOG_CRC_2"
        return
    fi
    log "$LOG_CRC_3"
    create_role_config $REPLICATION_ROLE_CONFIG
}

#
# Creating config file for policy request.
# Path to file: ./{$REPLICATION_ROLE_POLICY_CONFIG}
#
# Arguments: $1 - a name of config file
# Return   : None
#
create_role_policy_config() {
     LOG_CRRPC_1="=====> Creating new config file for policy [$1]..."
     LOG_CRRPC_2="<===== [!] Config file [./$1] is created."

     config_file="""
{
  \"Version\":\"2012-10-17\",
  \"Statement\":[
    {
      \"Effect\":\"Allow\",
      \"Action\":[
        \"s3:GetObjectVersionForReplication\",
        \"s3:GetObjectVersionAcl\"
      ],
      \"Resource\":[
        \"arn:aws:s3:::$SRC_BUCKET/*\"
      ]
    },
    {
      \"Effect\":\"Allow\",
      \"Action\":[
        \"s3:ListBucket\",
        \"s3:GetReplicationConfiguration\"
      ],
      \"Resource\":[
        \"arn:aws:s3:::$SRC_BUCKET\"
      ]
    },
    {
      \"Effect\":\"Allow\",
      \"Action\":[
        \"s3:ReplicateObject\",
        \"s3:ReplicateTags\",
        \"s3:GetObjectVersionTagging\"

      ],
      \"Resource\": [
        \"arn:aws:s3:::$DEST_BUCKET/*\"
      ]
    }
  ]
}
"""
    log "$LOG_CRRPC_1"
    echo $config_file > ./$1
    log "$LOG_CRRPC_2"
}

#
# Checking existing config file for policy.
# If not, it will be created automated.
#
# Arguments: $1 - a name of config file
# Return   : None
#
check_role_policy_config() {
    LOG_CRPC_1="====> Checking exist config file [./$1] for \
policy or not..."
    LOG_CRPC_2="<==== [!] Config file [./$1] for policy request already exists."
    LOG_CRPC_3="====> [!] Can't found file [./$1]."

    log "$LOG_CRPC_1"
    if [ -f "./$1" ];
    then
        log "$LOG_CRPC_2"
        return
    fi
    log "$LOG_CRPC_3"
    create_role_policy_config $1
}

#
# Creating role in AWS IAM.
# Asks user Approve before create it.
#
# Arguments: $1 - a role name
# Return   : None
#
create_role() {
    LOG_CRR_1="===> Creating replication role [$1] is started..."
    LOG_CRR_2="<=== [!] The role [$1] is created successfully."

    create_role_user_approving $1
    log "$LOG_CRR_1"
    check_role_config $REPLICATION_ROLE_CONFIG
    request_create_role $1 $REPLICATION_ROLE_CONFIG
    log "$LOG_CRR_2"
}

#
# Approving to enable bucket Versioning in AWS S3.
#
# Arguments: $1 - a bucket name
# Return   : None
#
enable_bucket_versioning_user_approving() {
    LOG_EBVUA_1="""Do you want enable versioning for bucket [$1]? \
It needs for the process of adding replication \
to bucket. If it doesn't exist, the process will stop."""
    LOG_EBVUA_2="[ERROR] Versioning in the bucket [$1] is disabled. \
You need to enable it to continue the process adding replication."

    if [ ! -z $WITHOUT_APPROVE ];
    then
        return
    fi

    res=$(approve_request "$LOG_EBVUA_1")
    if [ $res == "false" ];
    then
        echoerr "$LOG_EBVUA_2"
        exit 1
    fi
}

#
# Enable A Bucket Versioning.
# Asks user approve to enable it.
#
# Arguments: $1 - a bucket name
# Return   : None
#
enable_bucket_versioning() {
    LOG_EBV_1="====> Enabling a bucket versioning..."
    LOG_EBV_2="<==== [!] A bucket versioning for bucket [$1] is Enabled."

    enable_bucket_versioning_user_approving $1
    log "$LOG_EBV_1"
    request_enable_versioning $1
    log "$LOG_EBV_2"
}

#
# Checking bucket versioning.
#
# Arguments: $1 - a bucket name
# Return   : None
#
check_bucket_versioning() {
    LOG_CBV_1="==> Checking a bucket versioning for bucket [$1]..."
    LOG_CBV_2="===> Requesting versioning status for bucket [$1] to AWS S3..."
    LOG_CBV_3="<=== [!] Versioning in the bucket [$1] is already enabled."
    LOG_CBV_4="<=== [!] Versioning in the bucket [$1] is disabled."
    LOG_CBV_5="<== Checking is passed successfully."

    log "$LOG_CBV_1"
    log "$LOG_CBV_2"
    res=$(aws s3api get-bucket-versioning \
              --output text \
              --bucket $1)
    if [ ! -z $res ] && [ $res == "Enabled" ];
    then
        log "$LOG_CBV_3"
    else
        log "$LOG_CBV_4"
        enable_bucket_versioning $1	
    fi
}

#
# Checking bucket in list of buckets in AWS S3.
#
# Arguments: $1 - a bucket name.
# Return   : true or false
#
check_list_buckets() {
    s3_buckets=$( aws s3api list-buckets \
                      --output text \
                      --query "Buckets[].Name")
    for item in ${s3_buckets[@]}
    do
        if [ $item == $1 ];
        then
            echo "true"
            return
        fi
    done
    echo "false"
}

#
# Checking bucket in list of buckets.
# Checks list of buckets. If bucket
# doesn't exist, it will offer to create one.
#
# Arguments: $1 - a bucket name
# Return   : None
#
check_bucket() {
    LOG_CHB_1="==> Checking source bucket [$1]..."
    LOG_CHB_2="<== [!] The bucket [$1] already exists in AWS S3."
    LOG_CHB_3="<== [!] The bucket [$1] DOESN'T exist in AWS S3."

    log "$LOG_CHB_1"
    res=$(check_list_buckets $1)
    if [ $res == "true" ];
    then
        echoerr "$LOG_CHB_2"
        return
    fi
    log "$LOG_CHB_3"
    create_bucket $1
}

#
# Checking buckets.
# There are source and destination buckets in AWS S3 or not.
#
# Arguments: None
# Return   : None
#
check_buckets() {
    LOG_CHBS_1="=> Checking existing buckets in AWS S3..."
    LOG_CHBS_2="<= [!] Source and destination buckets \
are ready for replication."

    log "$LOG_CHBS_1"
    check_bucket $SRC_BUCKET
    check_bucket_versioning $SRC_BUCKET
    check_bucket $DEST_BUCKET
    check_bucket_versioning $DEST_BUCKET
    log "$LOG_CHBS_2"
}

#
# Attaching policy to replication role in AWS IAM.
#
# Arguments: $1 - a policy name
#            $2 - a role name
# Return   : None
#
attach_policy() {
    LOG_AP_1="===> Attaching policy [$1] to replication role [$2]..."
    LOG_AP_2="<=== [!] The policy [$1] is attached to \
replication role [$2] successfully."

    REPLICATION_ROLE_POLICY_CONFIG="$REPLICATION_ROLE_POLICY_CONFIG-$SRC_BUCKET.json"

    log "$LOG_AP_1"
    check_role_policy_config $REPLICATION_ROLE_POLICY_CONFIG
    request_attach_policy $REPLICATION_ROLE_POLICY_CONFIG $1 $2
    log "$LOG_AP_2"
}

#
# Checking list of attached policies to replication
# roles in AWS IAM.
# If not, it will be attached automated.
#
# Arguments: $1 - a policy name
#            $2 - a role name
# Return   : None
#
check_role_policy_attached() {
    LOG_CRPE_1="==> Checking policy [$1] in list of attached policies \
to replication role [$2] in AWS IAM..."
    LOG_CRPE_2="<== [!] The role [$1] is already attached."
    LOG_CRPE_3="<== [!] The role [$1] is NOT attached."

    log "$LOG_CRPE_1"
    iam_role_policies=$(aws iam list-role-policies \
                             --role-name $2 \
                             --output text \
                             --query "PolicyNames")
    for item in ${iam_role_policies[@]}
    do
        if [ $item == $1 ];
        then
            log "$LOG_CRPE_2"
            return
        fi
    done
    log "$LOG_CRPE_3"
    attach_policy $1 $2
}

#
# Checking existing roles in AWS IAM.
# Checks list of all roles. If role doesn't
# exist, it will offer to create one.
#
# Arguments: $1 - a role name
# Return   : None
#
check_role_existing() {
    LOG_CRE_1="==> Checking role in existing roles in AWS IAM..."
    LOG_CRE_2="<== [!] The replication role [$1] already exists in AWS IAM."
    LOG_CRE_3="<== [!] The replication role [$1] DOESN'T exist in AWS IAM."

    log "$LOG_CRE_1"
    iam_roles=$(aws iam list-roles \
                     --output text \
                     --query "Roles[].RoleName")
    for item in ${iam_roles[@]}
    do
        if [ $item == $1 ];
        then
            log "$LOG_CRE_2"
            return
        fi
    done
    log "$LOG_CRE_3"
    create_role $1
}

#
# Checking role.
# There is replication role for AWS S3 Replication or not.
#
# Arguments: None
# Return   : None
#
check_role() {
    REPLICATION_ROLE="$REPLICATION_ROLE-$SRC_BUCKET"
    REPLICATION_ROLE_POLICY="$REPLICATION_ROLE_POLICY-$SRC_BUCKET"
    LOG_CR_1="=> Checking replication role [$REPLICATION_ROLE] \
in AWS IAM..."
    LOG_CR_2="<= [!] Checking the replication role [$REPLICATION_ROLE] \
in AWS IAM is passed successfully."

    log "$LOG_CR_1"
    check_role_existing $REPLICATION_ROLE
    check_role_policy_attached $REPLICATION_ROLE_POLICY $REPLICATION_ROLE
    log "$LOG_CR_2"
}

#
# Checking empty bucket.
#
# Arguments: $1 - a bucket name.
# Return   : true or false
#
check_empty_bucket() {
    objects_list=$(aws s3 ls s3://$1)
    if [ ! -z "$objects_list" ];
    then
        echo "false"
        return
    fi
    echo "true"
}

#
# Approving to synchronize buckets in AWS S3.
# If user approve to synchronize bucket,
# it will be synchronized.
#
# Arguments: None
# Return   : None
#
sync_buckets_approving() {
    LOG_SBA_1="""Do you want to synchronize and copy all objects \
from a source bucket [$SRC_BUCKET] to a destination bucket \
[$DEST_BUCKET]? Be aware of replication rule works ONLY with \
new objects in the bucket."""
    LOG_SBA_2="<=== [!] Buckets were NOT synchronized."
    LOG_SBA_3="<=== [!] Buckets WERE synchronized and all objects copied."

    res=$(approve_request "$LOG_SBA_1")
    if [ $res == "false" ];
    then
        log "$LOG_SBA_2"
        return
    fi
    request_sync_buckets $SRC_BUCKET $DEST_BUCKET
    log "$LOG_SBA_3"
}

#
# Checking Bucket Size.
# Outputs information about buckets size:
#     - number of objects;
#     - bucket size (bytes).
#
# Arguments: $1 - a bucket name
# Return   : None
#
check_bucket_size() {
    LOG_CBS_1="====> Calculating buckets size..."
    LOG_CBS_2="<==== [!] A size information of bucket [$1]... "
    LOG_CBS_3="<=== [!] A bucket [$1] is EMPTY."

    res=$(check_empty_bucket $1)
    if [ $res == "false" ];
    then
        log "$LOG_CBS_1"
        bucket_size=($(request_bucket_size $1))
        log "$LOG_CBS_2"
        log "Objects: ${bucket_size[0]} | Bytes: ${bucket_size[1]}"
    else
        log "$LOG_CBS_3"
    fi
}

#
# Checking Buckets Size.
#
# Arguments: $1 - a source bucket name
#            $2 - a destination bucket name
# Return   : None
#
check_buckets_size() {
    LOG_CBZ_1="===> Checking buckets size before add replication rule..."
    LOG_CBZ_2="<=== [!] Buckets size is checked."

    log "$LOG_CBZ_1"
    check_bucket_size $1
    check_bucket_size $2
    log "$LOG_CBZ_2"
}

#
# Synchronizing Buckets before add replication rule.
# It will copy all data from $SRC_BUCKET to $DEST_BUCKET
#
# Arguments: None
# Return   : None
#
sync_buckets() {
    LOG_SB_1="==> The process of synchronizing data from source bucket "\
"[$SRC_BUCKET] to destination bucket [$DEST_BUCKET]..."
    LOG_SB_2="===> Checking empty a source bucket [$SRC_BUCKET]..."
    LOG_SB_3="<=== [!] A source bucket [$SRC_BUCKET] is NOT empty. \
There are some objects there."
    LOG_SB_4="<=== [!] A source bucket [$SRC_BUCKET] is empty. Nothing to copy."
    LOG_SB_5="<== [!] The process of Synchronizing data from source bucket "\
"[$SRC_BUCKET] to destination bucket [$DEST_BUCKET] is completed successfully."

    log "$LOG_SB_1"
    log "$LOG_SB_2"
    res=$(check_empty_bucket $SRC_BUCKET)
    if [ $res == "false" ];
    then
        log "$LOG_SB_3"
        sync_buckets_approving
    else
        log "$LOG_SB_4"
    fi
    log "$LOG_SB_5"
}

#
# Creating replication config file for request.
# Path to file: ./{$REPLICATION_ROLE_POLICY_CONFIG}
#
# Arguments: $1 - a name/path of config file
# Return   : None
#
create_replication_config() {
     LOG_CREPC_1="===> Creating new replication config file [$1]..."
     LOG_CREPC_2="<=== [!] Config file [./$1] is created."

     replication_role_iam_arn=$(aws --output text iam list-roles |
                                grep -w ROLES |
                                grep $REPLICATION_ROLE |
                                awk '{print $2}')

     config_file="""
{
  \"Role\": \"$replication_role_iam_arn\",
  \"Rules\": [
    {
      \"Status\": \"Enabled\",
      \"Priority\": 1,
      \"DeleteMarkerReplication\": { \"Status\": \"Disabled\" },
      \"Filter\" : {},
      \"Destination\": {
        \"Bucket\": \"arn:aws:s3:::$DEST_BUCKET\"
      }
    }
  ]
}
"""
    log "$LOG_CREPC_1"
    echo $config_file > ./$1
    log "$LOG_CREPC_2"
}

#
# Approving to create replication role in AWS IAM.
#
# Arguments: None
# Return   : None
#
add_replication_approving() {
    LOG_ARA_1="""Confirm please to add replication from \
source bucket [$SRC_BUCKET] to destination bucket [$DEST_BUCKET] in AWS S3! \
It needs for the process of adding replication \
to bucket. If it doesn't exist, the process will stop."""
    LOG_ARA_2="[!] The replication WASN'T ADDED to bucket [$SRC_BUCKET]. \
The process of adding replication was stopped."

    res=$(approve_request "$LOG_ARA_1")
    if [ $res == "false" ];
    then
        echoerr "$LOG_ARA_2"
        exit 1
    fi
}

#
# Checking exists or not replication config
# for request to AWS S3.
# If not, it will be created automated.
#
# Arguments: $1 - a name/path of config file
# Return   : None
#
check_replication_config() {
    LOG_CREC_1="==> Checking exists replication config file [./$1]..."
    LOG_CREC_2="<== [!] Config file [./$1] for replication request already exists."
    LOG_CREC_3="==> [!] Can't found config file [./$1]."

    log "$LOG_CREC_1"
    if [ -f "./$1" ];
    then
        log "$LOG_CREC_2"
        return
    fi
    log "$LOG_CREC_3"
    create_replication_config $1
}

#
# Checking Replication Status.
#
# Arguments: $1 - a bucket name
# Return   : None
#
check_replication() {
    LOG_CREP_1="==> Checking replication status for bucket [$1]..."
    LOG_CREP_2="<== [!] Replication for bucket [$1] is Enabled."

    log "$LOG_CREP_1"
    aws s3api get-bucket-replication \
    --bucket $1 \
    --profile $CLI_PROFILE
    log "$LOG_CREP_2"
}

#
# Adding Replication to AWS S3 $SRC_BUCKET Bucket.
#
# Arguments: None
# Return   : None
#
add_replication() {
    LOG_ARE_1="=> Adding S3 Replication from \
source bucket [$SRC_BUCKET] to destination bucket [$DEST_BUCKET]..."
    LOG_ARE_2="<= [!] S3 Replication to bucket [$SRC_BUCKET] \
is added successfully."

    REPLICATION_CONFIG="$REPLICATION_CONFIG-$SRC_BUCKET.json"

    log "$LOG_ARE_1"
    check_replication_config $REPLICATION_CONFIG
    add_replication_approving
    request_add_replication $REPLICATION_CONFIG $SRC_BUCKET
    check_replication $SRC_BUCKET
    log "$LOG_ARE_2"
}

#
# Main Function.
#
# Arguments: None
# Return   : None
#
main() {
    LOG_M_1="The process of adding AWS S3 Replication from \
bucket [$SRC_BUCKET] to bucket [$DEST_BUCKET] is started."
    LOG_M_2="[!] The process of adding AWS S3 Replication from \
bucket [$SRC_BUCKET] to bucket [$DEST_BUCKET] is completed successfully."
    LOG_M_3="[        AWS Connection        ]"
    LOG_M_4="[        AWS S3 Buckets        ]"
    LOG_M_5="[        AWS IAM Roles and Policies        ]"
    LOG_M_6="[        AWS S3 Buckets Synchronize        ]"
    LOG_M_7="[        AWS S3 Replication        ]"

    log "$LOG_M_1"
    log "$LOG_M_3"
    check_aws_connection
    log "$LOG_M_4"
    check_buckets
    log "$LOG_M_5"
    check_role
    log "$LOG_M_6"
    sync_buckets
    check_buckets_size $SRC_BUCKET $DEST_BUCKET
    log "$LOG_M_7"
    add_replication
    log "$LOG_M_2"
}


# --------------------| ARGUMENTS |--------------------

#
# Checking empty argument.
# If it isn't empty, the function will return an argument.
# Otherwise, it will show error message and usage.
#
# Arguments: $1 - argument
# Return   : argument
#
check_argument() {
    if [ ! -z $1 ];
    then
        echo $1
    else
        echoerr "[ERROR] There is an empty argument!"
        usage
    fi
}

#
# Arguments Parsing.
# Parses arguments and give error with usage
# if argument didn't find.
#
while [ $# -gt 0 ]; do
  case "$1" in
    --src-bucket)
      shift 1
      SRC_BUCKET=$(check_argument $1)
    ;;
    --dest-bucket)
      shift 1
      DEST_BUCKET=$(check_argument $1)
    ;;
    --aws-region)
      shift 1
      AWS_REGION=$(check_argument $1)
    ;;
    --cli-profile)
      shift 1
      CLI_PROFILE=$(check_argument $1)
    ;;
    --log-file-path)
      shift 1
      LOG_FILE_PATH=$(check_argument $1)
    ;;
    --without-approve)
      WITHOUT_APPROVE="true"
    ;;
    -h | --help)
      usage
    ;;
    *)
      echoerr "Unknown argument: $1"
      usage
    ;;
    esac
  shift 1
done

#
# Required Arguments Checking.
#
if [ -z $SRC_BUCKET ] || [ -z $DEST_BUCKET ];
then
    echoerr "[ERROR] Required arguments are --src-bucket and --dest-bucket."
    usage
fi

#
# Checking the same name for source and destination buckets.
#
if [ $SRC_BUCKET == $DEST_BUCKET ];
then
    echoerr "[ERROR] Replication to the same bucket is unavailable. \
Change the name of the destination bucket."
    exit 1
fi

main
